TARGET = psar_driver
OBJS = src/main.o src/malloc.o src/psar.o src/filetable.o src/crypto.o src/translation.o src/pspDecrypt_driver.o

CFLAGS = -Os -G0 -Wall -fno-pic
CXXFLAGS = $(CFLAGS) -fno-exceptions -fno-rtti
ASFLAGS = $(CFLAGS)

BUILD_PRX = 1
PRX_EXPORTS = src/exports.exp

USE_KERNEL_LIBC =1
USE_KERNEL_LIBS =1

LDFLAGS = -nostartfiles
LIBS = -lpspmesgd_driver -lpspnwman_driver -lpspsemaphore

LIBDIR = lib
INCDIR = include

PSPSDK=$(shell psp-config --pspsdk-path)
include $(PSPSDK)/lib/build.mak
