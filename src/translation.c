/*
Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 
*/

#include <pspsdk.h>
#include <pspkernel.h>
#include <pspdecrypt.h>

#include <string.h>
#include <stdio.h>

#include "main.h"

int IsTranslationTable(const char *fname)
{
	int x;
	
	/*  Old: com+01g+02g = 3
		New: 01g-12g     = 12
		---------------------
		Total            = 15 */
	for (x = 0; x < 15; x++)
	{
		char name[10];
		
		/* Old type (< 5.00) */
		if (x < 3)
		{
			if (x == 2)
			{
				/* Common table */
				strcpy(name, "com:00000");
			}
			
			else
			{
				/* 01g and 02g */
				sprintf(name, "%02ig:00000", x+1);
			}
		}
		
		/* New type (>= 5.00) */
		else
		{
			sprintf(name, "%05i", x-3+1);
		}
		
		/* Check the name */
		if (strcmp(fname, name) == 0)
		{
			return 1;
		}
	}
	
	return 0;
}

int GetTranslationTableModel(const char *fname)
{
	int x;
	int model;
    
	/*  Old: com+01g+02g = 3
		New: 01g-12g     = 12
		---------------------
		Total            = 15 */
	for (x = 0; x < 15; x++)
	{
		char name[10];
		
		/* Old type (< 5.00) */
		if (x < 3)
		{
			if (x == 2)
			{
				/* Common table */
				strcpy(name, "com:00000");
                
                // all models
                model = 0xFFFFFFFF;
			}
			
			else
			{
				/* 01g and 02g */
				sprintf(name, "%02ig:00000", x+1);
                
                model = x;
			}
		}
		
		/* New type (>= 5.00) */
		else
		{
			sprintf(name, "%05i", x-3+1);
            model = x - 3;
		}
        
		/* Check the name */
		if (strcmp(fname, name) == 0)
		{
			return model;
		}
	}
	
    // this is an error
	return 0;
}

int ImportTranslationTable(PSAR_Handler *handler, TranstableInfo *info, int *x, const char *filename)
{
	int res;
	
	FiletableItem *file = PSARFindFile(handler, filename, 0);
	
	if (!file)
	{
		return -1;
	}

	/* Allocate a buffer for the translation table */
	void *buf = (void *)_malloc(file->size+64);
	
	if (!buf)
	{
		return -2;
	}
	
	buf = (void *)((u32)buf&~63)+64;

	/* Grab the file containing the translation table */
	if ((res = PSARGetFile(handler, file, buf)) < 0)
	{
		return res;
	}
	
	/* Decrypt the table in-place */
	if ((res = CryptoDecryptTable(buf, file->size, handler->fw, (handler->version == 5))) < 0)
	{
		return res;
	}
	
	info[*x].transtable = buf;
	info[*x].transtable_size = res;
	info[*x].model = GetTranslationTableModel(filename);
    
	/* Increase the counter */
	(*x) ++;
	
	return 0;
}

void ClearTranslationTables(TranstableInfo *info, int x)
{
	int i;
	
	for (i = 0; i < x; i++)
	{
		_free(info[i].transtable);
	}
}

int RequiresResolving(const char *fname)
{
	int x;
	
	/* Check new type (>= 5.00) */
	if (strlen(fname) == 5)
	{
		for (x = 0; x < 5; x++)
		{
			if (fname[x] < '0' || fname[x] > '9')
			{
				break;
			}
		}
		
		if (x == 5)
		{
			return 1;
		}
	}

	/* Check old type (< 5.00) */
	for (x = 0; x < 3; x++)
	{
		char name[5];
		
		if (x == 2)
		{
			/* Common table */
			strcpy(name, "com:");
		}
		
		else
		{
			/* 01g and 02g */
			sprintf(name, "0%ig:", x+1);
		}
		
		if (strcmp(fname, name) == 0)
		{
			return 1;
		}
	}
	
	/* Doesn't require resolving */
	return 0;
}

static int FindTablePath(TranstableInfo *trans, char *number, char *szOut)
{
	// Taken from psardumper
	
	char *table = (char *)trans->transtable;
	int table_size = trans->transtable_size;
	
	int i, j, k;

	for (i = 0; i < table_size-5; i++)
	{
		if (strncmp(number, table+i, 5) == 0)
		{
			for (j = 0, k = 0; ; j++, k++)
			{
				if (table[i+j+6] < 0x20)
				{
					szOut[k] = 0;
					break;
				}

				if (table[i+5] == '|' && !strncmp(table+i+6, "flash", 5) &&
					j == 6)
				{
					szOut[6] = ':';
					szOut[7] = '/';
					k++;
				}
				else if (table[i+5] == '|' && !strncmp(table+i+6, "ipl", 3) &&
					j == 3)
				{
					szOut[3] = ':';
					szOut[4] = '/';
					k++;
				}
				else
				{				
					szOut[k] = table[i+j+6];
				}
			}

			return 1;
		}
	}

	return 0;
}

int GetFileModelCompatibility(TranstableInfo *info, int x, char *name)
{
    int models = 0;
	int i;
	char resolved[256];
    
	for (i = 0; i < x; i++)
	{
		if (FindTablePath(&info[i], name, resolved))
		{
            // check for common table
            if (info[i].model == 0xFFFFFFFF)
            {
                models = 0xFFFFFFFF;
            }
            
            models |= 1 << info[i].model;
		}
    }
    
    return models;
}

int Resolve(TranstableInfo *info, int x, char *name)
{
	int i;
	
	for (i = 0; i < x; i++)
	{
		if (FindTablePath(&info[i], name, name))
		{
			return 0;
		}
	}
	
	return -1;
}