/*
Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 
*/

#include <pspsdk.h>
#include <pspkernel.h>

#include <pspcrypt.h>
#include <pspdecrypt.h>

#include <string.h>

#include "main.h"

int CryptoDecryptTable(void *buf, int size, int fw, int pspgo)
{
	int table_mode;
	static short fws[] = { 0x000, 0x380, 0x400, 0x500, 0x570 };
	
	for (table_mode = sizeof(fws)/sizeof(short)-1; table_mode > -1; table_mode--)
	{
		if (fw >= fws[table_mode])
		{
			break;
		}
	}
	
	if (table_mode == 4 && fw < 0x630)
	{
		if (pspgo)
		{
			/* When the PSP Go was released, the table keys changed.
				Starting in 6.30 they are equal to the other models. */
			table_mode = 5;
		}
	}
	
	/* Use pspdecrypt.prx for now - plan: move this function in here entirely,
		there is no need for that function to be inside another PRX. */
	return pspDecryptTable(buf, buf, size, table_mode);
}

u8 demangle_before[16] =
{
	0xD8, 0x69, 0xB8, 0x95, 0x33, 0x6B, 0x63, 0x34,
	0x98, 0xB9, 0xFC, 0x3C, 0xB7, 0x26, 0x2B, 0xD7,
};

u8 demangle_after[16] =
{
	0x0D, 0xA0, 0x90, 0x84, 0xAF, 0x9E, 0xB6, 0xE2,
	0xD2, 0x94, 0xF2, 0xAA, 0xEF, 0x99, 0x68, 0x71,
};

static void PSPGo_Demangle_Step(u8 *buffer, u8 *key)
{
	int i;

	for (i = 0; i < 0x130; i++)
	{
		buffer[i] ^= key[i&0xF];
	}
}

int CryptoDemangle(void *outbuf, void *inbuf, int pspgo)
{
	u8 buf[sizeof(KIRK_CMD_HEADER)+0x130];
	memcpy(buf+sizeof(KIRK_CMD_HEADER), inbuf, 0x130);
	
	if (pspgo)
	{
		PSPGo_Demangle_Step(buf+sizeof(KIRK_CMD_HEADER), demangle_before);
	}
	
	KIRK_CMD_HEADER *hdr = (KIRK_CMD_HEADER *)buf;
	
	hdr->mode = 5;
	hdr->unk_4 = 0;
	hdr->unk_8 = 0;
	hdr->keyseed = 0x55;
	hdr->data_size = 0x130;
	
	int res = sceUtilsBufferCopyWithRange(buf, sizeof(buf), buf, sizeof(buf), 0x7);

	if (res < 0)
	{
		return res;
	}
	
	if (pspgo)
	{
		PSPGo_Demangle_Step(buf, demangle_after);
	}
	
	memcpy(outbuf, buf, 0x130);
	
	return 0;
}

int CryptoDecode(void *buf, int size)
{
	PSP_Header *psp_header = (PSP_Header *)buf;

	int outsize;
	int res = -1;

	if (psp_header->tag == 0x0E000000)
	{
		res = sceMesgd_driver_102DC8AF(buf, size, &outsize);
	}
	
	else if (psp_header->tag == 0x06000000)
	{
		res = sceNwman_driver_9555D68D(buf, size, &outsize);
	}
	
	else
	{
		/* Try to decrypt with pspdecrypt.prx (for now). */
		return pspDecryptPRX(buf, buf, size);
	}
	
	if (res < 0)
	{
		return res;
	}
	
	return outsize;
}