/*
Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 
*/

#ifndef ___PSAR_H___

#define ___PSAR_H___

#include "filetable.h"

/* PSAR handler */
typedef struct  __attribute__((__packed__))
{
	SceUID fd;
	char version;
	short fw;
	FiletableItem *ftable;
	int ftable_size;
} PSAR_Handler;

/* PSAR structures */
typedef struct
{
	u32 magic;
	int version;
	int size;
	u32 unk0;
} PSAR_Header;

typedef struct
{
	u32 unk0; //0
	char filename[252]; //4
	u32 unk1; //0x100
	int psp_size; //0x104
	int size; // 0x108
	char unk2; // 0x10C
	char unk3; // 0x10D
	char unk4; // 0x10E
	char signcheck; // 0x10F
} PSAR_File_Header;

/* Functions */
int PSAROpen(PSAR_Handler *handler, const char *path);
int PSARGetFile(PSAR_Handler *handler, FiletableItem *file, void *buf);
FiletableItem *PSARFindFile(PSAR_Handler *handler, const char *filename, int x);
int PSARClose(PSAR_Handler *handler);

#endif