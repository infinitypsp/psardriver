/*
Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 
*/

#include <pspsdk.h>
#include <pspkernel.h>
#include <pspsysmem_kernel.h>
#include <psputilsforkernel.h>

#include <string.h>

#include "main.h"

static int PSARGetHeader(SceUID fd, PSAR_Header *psar_header)
{
	while (1)
	{
		sceIoRead(fd, psar_header, sizeof(PSAR_Header));

		/* Check for '.PBP' */
		if (psar_header->magic == 0x50425000)
		{
			u32 psar_offset;
			
			sceIoLseek(fd, 0x24, PSP_SEEK_SET);
			sceIoRead(fd, &psar_offset, sizeof(psar_offset));
			
			sceIoLseek(fd, psar_offset, PSP_SEEK_SET);
		}
		
		/* Check for 'PSAR' */
		else if (psar_header->magic == 0x52415350)
		{
			return 0;
		}
		
		/* It's neither */
		else
		{
			return -1;
		}
	}
}

static int PSARGetBlockHeader(PSAR_Handler *handler, PSP_Header *psp_header, int blockpos)
{
	int res;
	
	/* Check if we need to seek elsewhere */
	if (blockpos > 0)
	{
		sceIoLseek(handler->fd, blockpos, PSP_SEEK_SET);
	}
	
	/* Read the block header */
	if ((res = sceIoRead(handler->fd, psp_header, sizeof(PSP_Header))) != sizeof(PSP_Header))
	{
		if (res >= 0)
		{
			res = -1;
		}
		
		return res;
	}
	
	/* In PSAR versions 2 and higher the header is mangled */
	if (handler->version >= 2)
	{
		CryptoDemangle((void *)psp_header+0x20, (void *)psp_header+0x20, (handler->version == 5));
	}
	
	return 0;
}

static int PSARGetBlockData(PSAR_Handler *handler, PSP_Header *psp_header, void *data, int size, int datapos)
{
	int res;
	int datasize = size-sizeof(PSP_Header);
	
	/* Check if we need to seek elsewhere */
	if (datapos > 0)
	{
		sceIoLseek(handler->fd, datapos, PSP_SEEK_SET);
	}
	
	/* Copy the header to the start of the buffer */
	memcpy(data, psp_header, sizeof(PSP_Header));
	
	/* Read the block data */
	if ((res = sceIoRead(handler->fd, data+sizeof(PSP_Header), datasize)) != datasize)
	{
		if (res >= 0)
		{
			res = -1;
		}
	
		return res;
	}
	
	return CryptoDecode(data, size);
}

FiletableItem *PSARFindFile(PSAR_Handler *handler, const char *filename, int x)
{
	return FiletableFindItem(handler->ftable, handler->ftable_size, filename, x);
}

int PSARGetFile(PSAR_Handler *handler, FiletableItem *file, void *buf)
{
	int res;
	PSP_Header psp_header;

	/* Read the ~PSP header of this file */
	if ((res = PSARGetBlockHeader(handler, &psp_header, file->pos)) < 0)
	{
		return res;
	}
	
	/* Allocate a buffer for the compressed data */
	int datasize = ((sizeof(PSP_Header)+psp_header.comp_size)&~15)+16;
	void *comp_buf = _malloc(datasize+64);
	
	if (!comp_buf)
	{
		return -1;
	}
	
	comp_buf = (void *)((u32)comp_buf&~63)+64;
	
	/* Read the compressed data */
	if ((res = PSARGetBlockData(handler, &psp_header, comp_buf, datasize, -1)) < 0)
	{
		return res;
	}
	
	/* Check for deflate header */
	if (((u8 *)comp_buf)[0] != 0x78 || ((u8 *)comp_buf)[1] != 0x9C)
	{
		return -2;
	}
	
	/* Decompress with deflate */
	res = sceKernelDeflateDecompress(buf, file->size, comp_buf+2, 0);
	
	if (res < 0)
	{
		return res;
	}
	
	/* Deallocate the compressed buffer */
	_free(comp_buf);
	
	return 0;
}

int PSAROpen(PSAR_Handler *handler, const char *path)
{
	int res;
	PSAR_Header psar_header;
	PSP_Header psp_header;
	
	/* Open the file */
	SceUID fd = sceIoOpen(path, PSP_O_RDONLY, 0);
	
	if (fd < 0)
	{
		return fd;
	}
	
	handler->fd = fd;
	
	/* Get the PSAR header */
	if ((res = PSARGetHeader(fd, &psar_header)) < 0)
	{
		return res;
	}
	
	handler->version = psar_header.version;
	
	/* Most data blocks are 0x150 (PSP_Header) + 0x110 =
		0x260 bytes and require an alignement to 64. */
	u8 _data[0x260+64];
	u8 *data = (u8 *)((u32)_data&~63)+64;
	
	/* Read the first block, containing version information */
	if ((res = PSARGetBlockHeader(handler, &psp_header, -1)) < 0)
	{
		return res;
	}

	if ((res = PSARGetBlockData(handler, &psp_header, data, 0x260, -1)) < 0)
	{
		return res;
	}
	
	/* Find and convert the firmware version */
	char *p = strrchr((char *)data+0x10, ',');
	
	if (!p)
	{
		return -1;
	}
	
	handler->fw = ((p[1]-'0') << 8) | ((p[3]-'0') << 4) | (p[4]-'0');
	
	/* In PSAR version 2 and higher there is a second block that we don't need */
	if (handler->version >= 2)
	{
		if ((res = PSARGetBlockHeader(handler, &psp_header, -1)) < 0)
		{
			return res;
		}
		
		/* Seek past it (16-alligned) */
		sceIoLseek(fd, (psp_header.comp_size+15)&~15, PSP_SEEK_CUR);
	}
	
	/* Get current position */
	int pos = sceIoLseek(fd, 0, PSP_SEEK_CUR);
	
	/* Transtable implementation */
	// Largest updater is the default update:
	// 01g+02g+03g+04g+06g+07g+08g+09g+15g+11g+12g = 11
	int info_x = 0;
	TranstableInfo info[11];
	
	
	/* Create a new filetable */
	handler->ftable = FiletableNew();
	handler->ftable_size = 0;
	
	while (1)
	{
		if (PSARGetBlockHeader(handler, &psp_header, -1) < 0)
		{
			break; // reached last file
		}
		
		/* Get the data of the file header */
		if ((res = PSARGetBlockData(handler, &psp_header, data, 0x260, -1)) < 0)
		{
			return res;
		}
		
		pos += 0x260;
		
		/* File header */
		PSAR_File_Header *entry = (PSAR_File_Header *)data;
//				printf("filename = %s\n", entry->filename);
		
        int models = 0;
        
		/* Resolve using translation table */
		if (RequiresResolving(entry->filename))
		{
            models = GetFileModelCompatibility(info, info_x, entry->filename);
			Resolve(info, info_x, entry->filename);
		}
		
		/* Add it to the filetable */
		handler->ftable_size += FiletableAddItem(handler->ftable, handler->ftable_size, entry->filename, entry->size, pos, entry->signcheck == 2, models);
		
		/* Check if it's a translation table, if it is we need it for later use */
		if (IsTranslationTable(entry->filename))
		{
			/* Import */
			ImportTranslationTable(handler, info, &info_x, entry->filename);

			/* ImportTranslationTable seeks elsewhere, return */
			sceIoLseek(fd, pos, PSP_SEEK_SET);
		}
		
		/* Skip past the file data */
		sceIoLseek(fd, entry->psp_size, PSP_SEEK_CUR);
		pos += entry->psp_size;
	}
	
	/* Add some directories that aren't in PSAR but are needed for drv implementation ... */
	int i;
	char *dirs[] = { "", "flash0", "flash1", "ipl",
					"flash0/data", "flash0/data/cert", "flash0/dic", "flash0/font", "flash0/kd",
					"flash0/vsh", "flash0/vsh/module", "flash0/vsh/resource", "flash0/vsh/etc" };
					
	// in 1.50 mkdir was done by updater and the dirs are not in psar
	int count = (handler->fw < 0x200) ? (sizeof(dirs)/sizeof(char *)) : (4);
	
	for (i = 0; i < count; i++)
	{
		handler->ftable_size += FiletableAddItem(handler->ftable, handler->ftable_size, dirs[i], 0, 0, 0, 0xFFFFFFFF);
	}

	/* Deallocate translation tables */
	ClearTranslationTables(info, info_x);
	
	/* Resize filetable */
	handler->ftable = FiletableResize(handler->ftable, handler->ftable_size);
	
	return 0;
}

int PSARClose(PSAR_Handler *handler)
{
	if (handler->ftable)
	{
		FiletableDelete(handler->ftable);
	}
	
	sceIoClose(handler->fd);
	handler->fd = -1;
	
	return 0;
}
