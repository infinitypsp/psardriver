/*
Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 
*/

#include <pspsdk.h>
#include <pspkernel.h>

#include <string.h>

#include "main.h"

FiletableItem *FiletableNew()
{
	return _malloc(32*1024);
}

FiletableItem *FiletableResize(FiletableItem *ftable, int ftable_size)
{
	void *data = _malloc(ftable_size);
	
	if (!data)
	{
		return NULL;
	}
	
	memcpy(data, ftable, ftable_size);
	_free(ftable);
	
	return data;
}

int FiletableAddItem(FiletableItem *ftable, int ftable_size, const char *filename, int size, int pos, char signcheck, int models)
{
	FiletableItem *last = (FiletableItem *)((u32)ftable+ftable_size);
	
	strcpy(&last->filename, filename);
	last->size = size;
	last->pos = pos;
    last->models = models;
    last->signcheck = signcheck;
	
	/* Example of what we are doing:
		flash0:/kd/loadcore.prx -> flash0/kd/loadcore.prx */
	char *colon = strchr(&last->filename, ':');
	
	if (colon)
		memmove(colon, colon+1, strlen(colon+1)+1);
		
	return sizeof(FiletableItem)+strlen(&last->filename);
}

FiletableItem *FiletableFindItem(FiletableItem *ftable, int ftable_size, const char *filename, int x)
{
	int i = 0;
	int filename_len = strlen(filename);

	while (i < ftable_size)
	{
		FiletableItem *p = (FiletableItem *)((u32)ftable+i);
		char *fname = &p->filename;

		/* With x = 0 it's single file */
		if (x == 0)
		{
			int len = strlen(filename);
			
			if (len > 0 && filename[len-1] == '/')
				len--;
			
			if (strncmp(fname, filename, len) == 0)
			{
				if (fname[len] == 0)
				{
					return p;
				}
			}
		}
		
		/* Otherwise it's directory listing */
		else
		{
			/* Get the file part */
			char *thefile = strrchr(fname, '/');
			
			/* Check the pre-file part */
			if ((!thefile) ?
				(strcmp(filename, "") == 0) :
				((filename_len == (thefile-fname)) && (strncmp(filename, fname, filename_len) == 0)))
			{
				if (strcmp(fname, "") != 0)
				{
					/* Reduce x until it is 1 to get the correct file :) */
					if (x == 1)
					{
						return p;
					}
					
					x--;
				}
			}
		}
	
		i += sizeof(FiletableItem)+strlen(&p->filename);
	}
	
	return NULL;
}

void FiletableDelete(FiletableItem *ftable)
{
	_free(ftable);
}