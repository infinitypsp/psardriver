/*
Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 
*/

#include <pspsdk.h>
#include <pspkernel.h>
#include <pspthreadman_kernel.h>

#include <string.h>

#include "main.h"

PSP_MODULE_INFO("psar_driver", 0x1006, 1, 0);

PspIoDrv psar_driver;
PSAR_Handler psar_handler;
SceIoStat psar_stat;

typedef struct  __attribute__((__packed__))
{
	u8 used;
	void *buf;
	int fileseek;
	FiletableItem *item;
} drv_file;

drv_file drv_files[32];

int IoInit(PspIoDrvArg* arg)
{
	memset(drv_files, 0, sizeof(drv_files));

	return 0;
}

int IoExit(PspIoDrvArg* arg)
{
	return 0;
}

int IoOpen(PspIoDrvFileArg *arg, char *file, int flags, SceMode mode)
{
	int i;
	int notallowed = (PSP_O_WRONLY | PSP_O_APPEND | PSP_O_CREAT | PSP_O_TRUNC | PSP_O_EXCL);
	
	if ((flags & notallowed) == notallowed)
	{
		/* invalid flags */
		return 0x8001B004;
	}
	
	for (i = 0; i < sizeof(drv_files)/sizeof(drv_file); i++)
	{
		if (!drv_files[i].used)
		{
			/* Find the file */
			FiletableItem *f = PSARFindFile(&psar_handler, file+1, 0);
			
			if (!f)
			{
				return SCE_KERNEL_ERROR_NOFILE;
			}
	
			/* Check if it's not a directory */
			if (!f->size)
			{
				return 0x80010015; // SCE_ERROR_ERRNO_EISDIR
			}
			
			/* Set info */
			drv_files[i].item = f;
			drv_files[i].fileseek = 0;

			/* Get the file and load it to the buffer */
			drv_files[i].buf = _malloc(f->size);
			
			if (!drv_files[i].buf)
			{
				return SCE_KERNEL_ERROR_NO_MEMORY;
			}
			
			int res;
			
			if ((res = PSARGetFile(&psar_handler, f, drv_files[i].buf)) < 0)
			{
				return res;
			}
			
			/* Store in drv arg */
			drv_files[i].used = 1;
			arg->arg = (void *)i;
			
			return 0;
		}
	}

	return 0x80010018; // SCE_ERROR_ERRNO_EMFILE
}

int IoClose(PspIoDrvFileArg *arg)
{
	int index = (int)arg->arg;
	
	/* Check if it is even opened */
	if (!drv_files[index].used)
	{
		return 0x80010009;
	}
	
	/* Check if it is a file */
	if (!drv_files[index].item->size)
	{
		/* it's not */
		return 0x80010009;
	}
	
	/* Free the buffer */
	_free(drv_files[index].buf);
	
	drv_files[index].used = 0;

	return 0;
}

int IoRead(PspIoDrvFileArg *arg, char *data, int len)
{
	int index = (int)arg->arg;
	
	/* Check if it is even opened */
	if (!drv_files[index].used)
	{
		return 0x80010009;
	}
	
	/* Check if it is a file */
	if (!drv_files[index].item->size)
	{
		/* it's not */
		return 0x80010009;
	}
	
	/* Copy the data over */
	int remaining = drv_files[index].item->size-drv_files[index].fileseek;
	
	if (len > remaining)
	{
		len = remaining;
	}
	
	memcpy(data, drv_files[index].buf+drv_files[index].fileseek, len);
	
	/* Move file pointer */
	drv_files[index].fileseek += len;
	
	return len;
}

SceOff IoLseek(PspIoDrvFileArg *arg, SceOff ofs, int whence)
{
	int index = (int)arg->arg;
	
	/* Check if file is even opened and a file (not a dir) */
	if (!drv_files[index].used || !drv_files[index].item->size)
	{
		return 0x80010009;
	}
	
	/* Move file pointer */
	if (whence == PSP_SEEK_SET)
	{
		drv_files[index].fileseek = (int)ofs;
	}
	
	else if (whence == PSP_SEEK_CUR)
	{
		drv_files[index].fileseek += (int)ofs;
	}
	
	else if (whence == PSP_SEEK_END)
	{
		drv_files[index].fileseek = drv_files[index].item->size-(int)ofs;
	}
	
	return drv_files[index].fileseek;
}

int IoIoctl(PspIoDrvFileArg *arg, unsigned int cmd, void *indata, int inlen, void *outdata, int outlen)
{
	int index = (int)arg->arg;
	
	/* Check if it is even opened */
	if (!drv_files[index].used)
	{
		return 0x80010009;
	}
	
	/* Check if it is a file */
	if (!drv_files[index].item->size)
	{
		/* it's not */
		return 0x80010009;
	}
	
    if (cmd == 0x00000001)
    {
        ((u32 *)outdata)[0] = drv_files[index].item->signcheck;
        return 0;
    }
    else if (cmd == 0x00000002)
    {
        ((u32 *)outdata)[0] = drv_files[index].item->models;
        return 0;
    }
    
	return SCE_KERNEL_ERROR_ERROR;
}

int IoDopen(PspIoDrvFileArg *arg, const char *dirname)
{
	int i;
	
	for (i = 0; i < sizeof(drv_files)/sizeof(drv_file); i++)
	{
		if (!drv_files[i].used)
		{
			/* Find the directory */
			FiletableItem *f = PSARFindFile(&psar_handler, dirname+1, 0);
			
			if (!f)
			{
				return SCE_KERNEL_ERROR_NOFILE;
			}
	
			/* Check if it's a directory */
			if (f->size)
			{
				return 0x80010014; // SCE_ERROR_ERRNO_ENOTDIR
			}
			
			/* Set info */
			drv_files[i].item = f;
			drv_files[i].fileseek = 0;

			/* Store in drv arg */
			drv_files[i].used = 1;
			arg->arg = (void *)i;
			
			return 0;
		}
	}
	
	return 0x80010018; // SCE_ERROR_ERRNO_EMFILE
}

int IoDclose(PspIoDrvFileArg *arg)
{
	int index = (int)arg->arg;
	
	/* Check if it is even opened */
	if (!drv_files[index].used)
	{
		return 0x80010009;
	}
	
	/* Check if it is a directory */
	if (drv_files[index].item->size)
	{
		/* it's not */
		return 0x80010009;
	}
	
	/* Close */
	drv_files[index].used = 0;

	return 0;
}

int IoDread(PspIoDrvFileArg *arg, SceIoDirent *dir)
{
	int index = (int)arg->arg;
	
	/* Check if it is even opened */
	if (!drv_files[index].used)
	{
		return 0x80010009;
	}
	
	/* Check if it is a directory */
	if (drv_files[index].item->size)
	{
		/* it's not */
		return 0x80010009;
	}
	
	if (drv_files[index].fileseek >= 2)
	{
		/* Get file at current seek counter */
		FiletableItem *f = PSARFindFile(&psar_handler, &drv_files[index].item->filename, drv_files[index].fileseek-2+1);
		
		if (!f)
		{
			/* no more files left */
			return 0;
		}
		
		/* Copy last part (file only) */
		char *p = strrchr(&f->filename, '/');
		
		if (!p)
		{
			p = &f->filename;
		}
		
		else
		{
			p++;
		}
		
		strncpy(dir->d_name, p, sizeof(dir->d_name));
		
		/* Set the stat */
		memcpy(&dir->d_stat, &psar_stat, sizeof(SceIoStat));
		
		dir->d_stat.st_size = f->size;
		dir->d_stat.st_mode = (f->size) ? (FIO_S_IFREG) : (FIO_S_IFDIR);
	}
	
	else
	{
		strcpy(dir->d_name, (drv_files[index].fileseek == 0) ? "." : "..");
		
		/* Set the stat */
		memcpy(&dir->d_stat, &psar_stat, sizeof(SceIoStat));
		
		dir->d_stat.st_size = 0;
		dir->d_stat.st_mode = FIO_S_IFDIR;
	}
	
	/* Increase seek counter */
	drv_files[index].fileseek ++;
	
	return 1;
}

int IoGetstat(PspIoDrvFileArg *arg, const char *file, SceIoStat *stat)
{
	/* Find the file */
	FiletableItem *f = PSARFindFile(&psar_handler, file+1, 0);
	
	if (!f)
	{
		return SCE_KERNEL_ERROR_NOFILE;
	}
	
	/* Set the stat */
	memcpy(stat, &psar_stat, sizeof(SceIoStat));
	
	stat->st_size = f->size;
	stat->st_mode = (f->size) ? (FIO_S_IFREG) : (FIO_S_IFDIR);

	return 0;
}

int IoChdir(PspIoDrvFileArg *arg, const char *dir)
{
	return 0;
}

int IoMount(PspIoDrvFileArg *arg)
{
	return 0;
}

int IoUmount(PspIoDrvFileArg *arg)
{
	return 0;
}

int _IoDevctl(u32 *argv)
{
	unsigned int cmd = argv[0];
	void *indata = (void *)argv[1];

	int res;
	
	if (cmd == 0x00000001)
	{
		if (psar_handler.fd >= 0)
		{
			PSARClose(&psar_handler);
		}
		
		if ((res = PSAROpen(&psar_handler, (const char *)indata)) < 0)
		{
			return res;
		}
		
		memset(&psar_stat, 0, sizeof(SceIoStat));
		sceIoGetstat((const char *)indata, &psar_stat);
		
		return 0;
	}
	
	else if (cmd == 0x00000002)
	{
		if ((res = PSARClose(&psar_handler)) < 0)
		{
			return res;
		}
		
		return 0;
	}
	
	return SCE_KERNEL_ERROR_ERROR;
}

int IoDevctl(PspIoDrvFileArg *arg, const char *devname, unsigned int cmd, void *indata, int inlen, void *outdata, int outlen)
{
	u32 argv[2];
	
	argv[0] = (u32)cmd;
	argv[1] = (u32)indata;

	return sceKernelExtendKernelStack(0x4000, (void *)_IoDevctl, argv);
}

PspIoDrvFuncs psar_funcs =
{
	IoInit,
	IoExit,
	IoOpen,
	IoClose,
	IoRead,
	NULL, /* IoWrite */
	IoLseek,
	IoIoctl,
	NULL, /* IoRemove */
	NULL, /* IoMkdir */
	NULL, /* IoRmdir */
	IoDopen,
	IoDclose,
	IoDread,
	IoGetstat,
	NULL, /* IoChstat */
	NULL, /* IoRename */
	IoChdir,
	IoMount,
	IoUmount,
	IoDevctl,
	NULL,
};

PspIoDrv psar_driver = { "psar", 0x10, 0x800, "PSAR", &psar_funcs };

int module_start(SceSize args, void *argp)
{
	psar_handler.fd = -1;
	
	if (sceIoAddDrv(&psar_driver) < 0)
	{
		return 1;
	}

	return 0;
}

int module_stop(SceSize args, void *argp)
{
	if (psar_handler.fd >= 0)
	{
		PSARClose(&psar_handler);
	}
	
	return 0;
}
